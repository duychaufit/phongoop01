class Services{
	public: void fence(){
		cout<<"----------------------------------"<<endl;
	}

	public: int input_age_validate(){
		int input_age;
		bool valid = false;
		do{
			cout<<"Age : ";
			cin>>input_age;
			if(cin.good()){
				valid = true;
				break;
			}
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(),'\n');
      cout << "Please insert a number!" << endl;
		}while(!valid);
		return input_age;
	}

	public: bool input_sexual_validate(){
		bool input_sexual;
		bool valid = false;
		do{
			cout<<"Sexual : ";
			cin>>input_sexual;
			if(cin.good()){
				valid = true;
				break;
			}
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(),'\n');
      cout << "0 is female, 1 is male" << endl;
		}while(!valid);
	return input_sexual;
	}

	public: int input_weight_validate(){
		int input_weight;
		bool valid = false;
		do{
			cout<<"Weight : ";
			cin>>input_weight;
			if(cin.good()){
				valid = true;
				break;
			}
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(),'\n');
      cout << "Please insert a number!" << endl;
		}while(!valid);
		return input_weight;
	}

	public:int input_menu_validate(){
		int input_menu;
		bool valid = false;
		do{
			cout<<"Insert : ";
			cin>>input_menu;
			if(cin.good()){
				valid = true;
				break;
			}
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(),'\n');
      cout << "Please insert a number of menu!" << endl;
		}while(!valid);
	return input_menu;
	}

	public: bool input_birdsong_validate(){
		bool input_birdsong;
		bool valid = false;
		do{
			cout<<"Birdsong : ";
			cin>>input_birdsong;
			if(cin.good()){
				valid = true;
				break;
			}
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(),'\n');
      cout << "Please insert only 0 or 1" << endl;
		}while(!valid);
	return input_birdsong;
	}

	public: bool input_mane_validate(){
		bool input_mane;
		bool valid = false;
		do{
			cout<<"Mane : ";
			cin>>input_mane;
			if(cin.good()){
				valid = true;
				break;
			}
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(),'\n');
      cout << "Please insert only 0 or 1" << endl;
		}while(!valid);
	return input_mane;
	}
};
