class Menu{
  Services* service;

  public: int animal_category_menu(){
    int animal_category_menu;
    cout<<"1.Bird"<<endl;
    cout<<"2.Lion"<<endl;
    cout<<"3.Exit"<<endl;
    service->fence();
    animal_category_menu = service->input_menu_validate();
    service->fence();
    return animal_category_menu;
  }

  Birdservices* birdservice;
  public: int bird_detail_menu(){
    int bird_detail_menu = 0;
    bool bird_detail_menu_flag = true;
    while(bird_detail_menu_flag){
      cout<<"1.Name"<<endl;
      cout<<"2.Age"<<endl;
      cout<<"3.Sexual"<<endl;
      cout<<"4.Weight"<<endl;
      cout<<"5.Feather"<<endl;
      cout<<"6.Birdsong"<<endl;
      cout<<"7.All infor"<<endl;
      cout<<"8.Back"<<endl;
      service->fence();
      cin.clear();
      cin.ignore();
      bird_detail_menu = service->input_menu_validate();;
      service->fence();
      switch(bird_detail_menu){
        case 1:{
          birdservice->bird_list_show_name();
          break;
        }
        case 2:{
          birdservice->bird_list_show_age();
          break;
        }
        case 3:{
          birdservice->bird_list_show_sexual();
          break;
        }
        case 4:{
          birdservice->bird_list_show_weight();
          break;
        }
        case 5:{
          birdservice->bird_list_show_feather();
          break;
        }
        case 6:{
          birdservice->bird_list_show_birdsong();
          break;
        }
        case 7:{
          birdservice->bird_list_show();
          break;
        }
        case 8:{
          bird_detail_menu_flag = false;
          break;
        }
        default:{
          cout<<"Please insert a number of menu!"<<endl;
          service->fence();
          break;
        }
      }
    }
    return bird_detail_menu;
  }

  public: int bird_menu(){
    int bird_menu = 0;
    bool bird_flag = true;
    while(bird_flag){
      cout<<"1.Add"<<endl;
      cout<<"2.Show"<<endl;
      cout<<"3.Delete"<<endl;
      cout<<"4.Back"<<endl;
      service->fence();
      cin.clear();
      cin.ignore();
      bird_menu = service->input_menu_validate();
      service->fence();
      switch(bird_menu){
        case 1:{
          birdservice->add_bird_to_list();
          service->fence();
          break;
        }
        case 2:{
          bird_detail_menu();
          break;
        }
        case 3:{
          birdservice->remove_bird();
          service->fence();
          break;
        }
        case 4:{
          bird_flag = false;
          break;
        }
        default:{
          cout<<"Please insert a number of menu!"<<endl;
          service->fence();
          break;
        }
      }
    }
    return bird_menu;
  }

  Lionservices* lionservice;
  public:int lion_detail_menu(){
    int lion_detail_menu = 0;
    bool lion_detail_menu_flag = true;
    while(lion_detail_menu_flag){
      cout<<"1.Name"<<endl;
      cout<<"2.Age"<<endl;
      cout<<"3.Sexual"<<endl;
      cout<<"4.Weight"<<endl;
      cout<<"5.Mane"<<endl;
      cout<<"6.All infor"<<endl;
      cout<<"7.Back"<<endl;
      service->fence();
      cin.clear();
      cin.ignore();
      lion_detail_menu = service->input_menu_validate();
      service->fence();
      switch(lion_detail_menu){
        case 1:{
          lionservice->lion_list_show_name();
          break;
        }
        case 2:{
          lionservice->lion_list_show_age();
          break;
        }
        case 3:{
          lionservice->lion_list_show_sexual();
          break;
        }
        case 4:{
          lionservice->lion_list_show_weight();
          break;
        }
        case 5:{
          lionservice->lion_list_show_mane();
          break;
        }
        case 6:{
          lionservice->lion_list_show();
          break;
        }
        case 7:{
          lion_detail_menu_flag = false;
          break;
        }
        default:{
          cout<<"Please insert a number of menu!"<<endl;
          service->fence();
          break;
        }
      }
    }
    return lion_detail_menu;
  }

  public:int lion_menu(){
    int lion_menu = 0;
    bool lion_flag = true;
    while(lion_flag){
      cout<<"1.Add"<<endl;
      cout<<"2.Show"<<endl;
      cout<<"3.Delete"<<endl;
      cout<<"4.Back"<<endl;
      service->fence();
      cin.clear();
      cin.ignore();
      lion_menu = service->input_menu_validate();
      service->fence();
      switch(lion_menu){
        case 1:{
          lionservice->add_lion_to_list();
          service->fence();
          break;
        }
        case 2:{
          lion_detail_menu();
          break;
        }
        case 3:{
          lionservice->remove_lion();
          service->fence();
          break;
        }
        case 4:{
          lion_flag = false;
          break;
        }
        default:{
          cout<<"Please insert a number of menu!"<<endl;
          service->fence();
          break;
        }
      }  
    }
    return lion_menu;
  }
};
