class Animal{
	protected: string name;
	protected: int age;
	protected: bool sexual;
	protected: int weight;
  //set
  protected: void animal_init(string inputName, int inputAge, bool inputSexual, int inputWeight){
    name = inputName;
    age = inputAge;
    sexual = inputSexual;
    weight = inputWeight;
  }
  //get
  public: string getName(){
    return name;
  }
  public: int getAge(void){
    return age;
  }
  public: bool getSexual(void){
    return sexual;
  }
  public: int getWeight(void){
    return weight;
  }
  //print out
  public: void printName(){
    cout<<"Name : "<<name<<endl;
  }
  public: void printAge(){
    cout<<"Age : "<<age<<" year old"<<endl;
  }
  public: void printSexual(){
    if(sexual == 0){
      cout<<"Sexual : Female"<<endl;
    }
    if(sexual == 1){
      cout<<"Sexual : Male"<<endl;
    }
  }
  public: void printWeight(){
    cout<<"Weight : "<<weight<<" Kg"<<endl;
  }
  protected: void printAll(){
    printName();
    printAge();
    printSexual();
    printWeight();
  }
};
