class Bird: public Animal{
  Services* service;
  private: string feathers;
	private: bool birdsong;
	
	public: Bird(string inputName, int inputAge, bool inputSexual, int inputWeight, string inputFeathers, bool inputBirdsong){
		Animal::animal_init(inputName, inputAge, inputSexual, inputWeight);
		feathers = inputFeathers;
		birdsong = inputBirdsong;
	}
  public: string getFeathers(){
    return feathers;
  }
  public: bool getBirdsong(){
    return birdsong;
  }
  
  public: void printBirdsong(){
    if(birdsong == 0){
      cout<<"BirdSong : No"<<endl;
    }
    if(birdsong == 1){
      cout<<"BirdSong : Have"<<endl;
    }
  }

  public: void printFeathers(){
    cout<<"Feather : "<<feathers<<endl;
  }
  
  // @override
  public: void printAll(){
    Animal::printAll();
    printFeathers();
    printBirdsong();
    service->fence();
  }
};
