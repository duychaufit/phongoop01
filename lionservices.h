class Lionservices: public Services{
  public: Lion* lion_init(){
    Services* service;
    cout<<"Name : ";
    string inputName;
    cin>>inputName;

    int inputAge = service->input_age_validate();

    bool inputSexual = service->input_sexual_validate();;

    int inputWeight = service->input_weight_validate();

    bool inputMane = service->input_mane_validate();
    service->fence();

    Lion* sutu = new Lion(inputName, inputAge, inputSexual, inputWeight, inputMane);
    
    return sutu;
  }

  //Default lion list
  public: void default_lion_list(){
    Lion* consutu = new Lion("Simba", 1, 1, 60, 1);
    Zoo::lion_list.push_back(consutu);
    consutu = new Lion("Mufasa", 2, 1, 65, 1);
    Zoo::lion_list.push_back(consutu);
    consutu = new Lion("Sarafina", 3, 0, 70, 0);
    Zoo::lion_list.push_back(consutu);
  }

  //Add lion
  public: void add_lion_to_list(){
    Lion* sutu = lion_init();
    Zoo::lion_list.push_back(sutu);
    cout<<"A lion had been added"<<endl;
  }

  //Show lion detail
  private: void none_lion(){
    if(Zoo::lion_list.size() == 0){
      cout<<"Still have no one yet"<<endl;
    }
  }
  public: void lion_list_show_name(){
    none_lion();
    for(int lion_num = 0; lion_num < Zoo::lion_list.size(); lion_num++){
      Lion* sutu = Zoo::lion_list.at(lion_num);
      cout<<lion_num +1<<".";
      sutu->printName();
    }
    Services::fence();
  }
  public: void lion_list_show_age(){
    none_lion();
    for(int lion_num = 0; lion_num < Zoo::lion_list.size(); lion_num++){
      Lion* sutu = Zoo::lion_list.at(lion_num);
      cout<<lion_num +1<<".";
      sutu->printAge();
    }
    Services::fence();
  }
  public: void lion_list_show_sexual(){
    none_lion();
    for(int lion_num = 0; lion_num < Zoo::lion_list.size(); lion_num++){
      Lion* sutu = Zoo::lion_list.at(lion_num);
      cout<<lion_num +1<<".";
      sutu->printSexual();
    }
    Services::fence();
  }
  public: void lion_list_show_weight(){
    none_lion();
    for(int lion_num = 0; lion_num < Zoo::lion_list.size(); lion_num++){
      Lion* sutu = Zoo::lion_list.at(lion_num);
      cout<<lion_num +1<<".";
      sutu->printWeight();
    }
    Services::fence();
  }
  public: void lion_list_show_mane(){
    none_lion();
    for(int lion_num = 0; lion_num < Zoo::lion_list.size(); lion_num++){
      Lion* sutu = Zoo::lion_list.at(lion_num);
      cout<<lion_num +1<<".";
      sutu->printMane();
    }
    Services::fence();
  }
  public: void lion_list_show(){
    none_lion();
    for(int lion_num = 0; lion_num < Zoo::lion_list.size(); lion_num++){
      Lion* sutu = Zoo::lion_list.at(lion_num);
      cout<<lion_num +1<<"."<<endl;
      sutu->printAll();
    }
  }

  //Remove lion
  public: void remove_lion(){
    for(int lion_num = 0; lion_num < Zoo::lion_list.size(); lion_num++){
      Lion* sutu = Zoo::lion_list.at(lion_num);
      cout<<lion_num +1<<"."<<endl;
      sutu->printAll();
    }
    cout<<"Insert 0 to back"<<endl;
    cout<<"Insert number of lion wanna remove : ";
    int remove_lion;
    cin>>remove_lion;
    Services::fence();
    while(remove_lion < 0 || remove_lion > Zoo::lion_list.size()){
      cout<<"Inserted number out of list!"<<endl;
      cout<<"Insert again : ";
      cin>>remove_lion;
      Services::fence();
    }
    if(remove_lion != 0){
      Zoo::lion_list.erase(Zoo::lion_list.begin() + (remove_lion - 1));
      cout<<"Number "<<remove_lion<<" of lion has been removed"<<endl;
    }
    if(remove_lion == 0){
      cout<<"Turned back"<<endl;
    }
  }

};
