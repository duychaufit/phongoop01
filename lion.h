class Lion: public Animal{
  Services* service;
	private: bool mane;
	
	public: Lion(string inputName, int inputAge, bool inputSexual, int inputWeight, bool inputMane){
		Animal::animal_init(inputName, inputAge, inputSexual, inputWeight);
		mane = inputMane;
	}
  public: bool getMane(){
    return mane;
  }
  public: void printMane(){
    if(mane == 0){
      cout<<"Mane : No"<<endl;
    }
    if(mane == 1){
      cout<<"Mane : Yes"<<endl;
    }
  }
  public: void printAll(){
    Animal::printAll();
    printMane();
    service->fence();
  }
};
