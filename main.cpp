#include "include.h"

vector<Bird*> Zoo::bird_list;
vector<Lion*> Zoo::lion_list;

int main(){
	Birdservices* default_bird_list = new Birdservices();
	default_bird_list->default_bird_list();
	Lionservices* default_lion_list = new Lionservices();;
	default_lion_list->default_lion_list();
	Services* service;

	Menu* program_menu = new Menu();
	while(true){
		switch(program_menu->animal_category_menu()){
			case 1:{
				program_menu->bird_menu();
				break;
			}
			case 2:{
				program_menu->lion_menu();
				break;
			}
			case 3:{
				cout<<"Good bye!"<<endl;
				return 0;
			}
			default:{
				cout<<"Please insert a number of menu!"<<endl;
				service->fence();
				break;
			}
		}
	}
	return 0;
}
