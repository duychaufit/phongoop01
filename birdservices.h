class Birdservices: public Services{
  public: Bird* bird_init(){
    Services* service;
    cout<<"Name : ";
    string inputName;
    cin>>inputName;

    int inputAge = service->input_age_validate();

    bool inputSexual = service->input_sexual_validate();;

    int inputWeight = service->input_weight_validate();

    cout<<"Feather : ";
    string inputFeather;
    cin>>inputFeather;

    bool inputBirdsong = service->input_birdsong_validate();
    Services::fence();

    Bird* chim = new Bird(inputName, inputAge, inputSexual, inputWeight, inputFeather, inputBirdsong);
    
    return chim;
  }

  //Default bird list
  public: void default_bird_list(){
    Bird* chim = new Bird("Horus", 1, 0, 2, "Black Orange", 0);
    Zoo::bird_list.push_back(chim);
    chim = new Bird("Nakee", 2, 1, 2, "Cyan Blue", 0);
    Zoo::bird_list.push_back(chim);
    chim = new Bird("Venus", 3, 0, 2, "White Pink", 1);
    Zoo::bird_list.push_back(chim);
  }

  //Add bird
  public: void add_bird_to_list(){
    Bird* chim = bird_init();
    Zoo::bird_list.push_back(chim);
    cout<<"A bird had been added"<<endl;
  }

  //Show bird detail
  private: void none_bird(){
    if(Zoo::bird_list.size() == 0){
      cout<<"Still have no one yet"<<endl;
    }
  }
  public: void bird_list_show_name(){
    none_bird();
    for(int bird_num = 0; bird_num  < Zoo::bird_list.size(); bird_num++){
      Bird* chim = Zoo::bird_list.at(bird_num);
      cout<<bird_num + 1<<".";
      chim->printName();
    }
    Services::fence();
  }
  public: void bird_list_show_age(){
    none_bird();
    for(int bird_num = 0; bird_num < Zoo::bird_list.size(); bird_num++){
      Bird* chim = Zoo::bird_list.at(bird_num);
      cout<<bird_num + 1<<".";
      chim->printAge();
    }
    Services::fence();
  }
  public: void bird_list_show_sexual(){
    none_bird();
    for(int bird_num = 0; bird_num < Zoo::bird_list.size(); bird_num++){
      Bird* chim = Zoo::bird_list.at(bird_num);
      cout<<bird_num + 1<<".";
      chim->printSexual();
    }
    Services::fence();
  }
  public: void bird_list_show_weight(){
    none_bird();
    for(int bird_num = 0; bird_num < Zoo::bird_list.size(); bird_num++){
      Bird* chim = Zoo::bird_list.at(bird_num);
      cout<<bird_num + 1<<".";
      chim->printWeight();
    }
    Services::fence();
  }
  public: void bird_list_show_feather(){
    none_bird();
    for(int bird_num = 0; bird_num < Zoo::bird_list.size(); bird_num++){
      Bird* chim = Zoo::bird_list.at(bird_num);
      cout<<bird_num + 1<<".";
      chim->printFeathers();
    }
    Services::fence();
  }
  public: void bird_list_show_birdsong(){
    none_bird();
    for(int bird_num = 0; bird_num < Zoo::bird_list.size(); bird_num++){
      Bird* chim = Zoo::bird_list.at(bird_num);
      cout<<bird_num +1<<".";
      chim->printBirdsong();
    }
    Services::fence();
  }
  public: void bird_list_show(){
    none_bird();
    for(int bird_num = 0; bird_num < Zoo::bird_list.size(); bird_num++){
      Bird* chim = Zoo::bird_list.at(bird_num);
      cout<<bird_num +1<<"."<<endl;
      chim->printAll();
    }
  }

  //Remove bird
  public: void remove_bird(){
    for(int bird_num = 0; bird_num < Zoo::bird_list.size(); bird_num++){
      Bird* chim = Zoo::bird_list.at(bird_num);
      cout<<bird_num +1<<"."<<endl;
      chim->printAll();
    }
    cout<<"Insert 0 to back"<<endl;
    cout<<"Insert number of bird wanna remove : ";
    int remove_bird;
    cin>>remove_bird;
    Services::fence();
    while(remove_bird < 0 || remove_bird > Zoo::bird_list.size()){
      cout<<"Inserted number out of list!"<<endl;
      cout<<"Insert again : ";
      cin>>remove_bird;
      Services::fence();
    }
    if(remove_bird != 0){
      Zoo::bird_list.erase (Zoo::bird_list.begin() + (remove_bird - 1));
      cout<<"Number "<<remove_bird<<" of bird has been removed"<<endl;  
    }
    if(remove_bird == 0){
      cout<<"Turned back"<<endl;
    }
  }
};
